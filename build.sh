#!/bin/bash
set -e

# Pulling necessary base images
echo "Pulling necessary base images"
docker pull harshjv/mysql:0.9.16-1.0
docker pull harshjv/composer-data:0.9.16-1.0
docker pull harshjv/php-nginx:0.9.16-1.0

# Create log & data folder
echo "Create log & data folder"
mkdir -p log
mkdir -p data

# Run docker containers in the background
echo "Run docker containers in the background"
docker-compose up -d

# Install dependences
echo "Installing dependences"
docker-compose run --rm phpnginx php composer.phar install

# Create database
sleep 10
echo "Creating database"
docker-compose run --rm mysql mysql -hmysql --password=root -e "CREATE DATABASE gotoba CHARACTER SET utf8 COLLATE utf8_unicode_ci"

# Install dependences
echo "Installing dependences"
docker-compose run --rm phpnginx php artisan migrate