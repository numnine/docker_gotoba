<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Challenge extends Model 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prediction_id', 'nickname',
    ];
}
