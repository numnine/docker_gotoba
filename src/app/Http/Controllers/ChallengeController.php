<?php

namespace App\Http\Controllers;
use App\Challenge as Challenge;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{

    protected $challenge;

    public function __construct(Challenge $challenge)
    {
        $this->challenge = $challenge;
    }

    public function show($id)
    {
        $challenge = $this->challenge->find($id);
        if(!$challenge) {
            abort(404);
        }
        return $challenge->toJson();
    }

    public function store(Request $request)
    {
        $this->challenge->result = 1;
        $this->challenge->nickname = $request->input('nickname');
        $this->challenge->prediction_id = $request->input('prediction_id');
        $this->challenge->save();

        return $this->challenge->toJson();
    }
    
}
